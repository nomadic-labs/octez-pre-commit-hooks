#!/bin/sh

cargo_dominators() {
    dominator=Cargo.toml

    # find all Cargo.toml files
    # for each input. find the longest matching prefix.
    cargos=$(
        find . -iname _build -prune -o -iname _opam -prune -o -iname "${dominator}" -print |
            xargs dirname |
            awk '{ print length, $0 }' | sort -rn | cut -d" " -f2-
    )
    while read -r dir; do
        dir="./${dir}"
        f=""
        for c in $cargos; do
            # check if c is a prefix of dir
            if [ "${dir%"${c}"*}" != "$dir" ]; then
                f="${c}/${dominator}"
                break
            fi
        done
        if [ -n "$f" ]; then
            echo "$f"
        fi
    done
}

timeout=${TIMEOUT:-0.5s}
rustup default "$(cat rust-toolchain)" > /dev/null 2>&1
for crate in $(dirname "$@" | sort -u | cargo_dominators); do
    echo "$(dirname "$crate"): cargo check"
    cd "$(dirname "$crate")" || exit 1
    timeout "${timeout}" cargo check
    exit_status="$?"
    if [ ${exit_status} = 124 ]; then
        echo "Command timed out (TIMEOUT = ${TIMEOUT})"
    else
        exit ${exit_status}
    fi
    cd - || exit 1
done
